require 'test_helper'

class AminTest < ActionDispatch::IntegrationTest

  test "admin/ link should work" do
    get "/admin"
    assert_response :success
  end

  test "admin/user link should work" do
    get admin_user_path
    assert_response :success
  end

  
end
