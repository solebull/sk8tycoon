require 'test_helper'

class MaintenanceModeTest < ActionDispatch::IntegrationTest
  include MaintenanceHelper # Uses in_maintenance()

  test "If no maintenance record return false" do

    
    assert_not in_maintenance
    # FXIME: this test throws a =undefined method `content_type'= error
    #    assert_select "div#in-maintenance", false

  end

  test "If a maintenance record returns true" do
    m = Maintenance.create!({comment: "test", start: nil, duration: nil})
    assert in_maintenance

    # FXIME: this test throws a =undefined method `content_type'= error
    #    assert_select "h1", {count: 1, text: "Maintenance mode"}
    #    assert_select "div#in-maintenance", 1
    
    m.delete
  end

  test "Hasn't instant maintenance" do
    assert_not has_instant_maintenance
  end

  test "Has instant maintenance" do
    m = Maintenance.create!({comment: "test", start: nil, duration: nil})
    assert has_instant_maintenance
    m.delete
  end

  test "Has a #in() function" do
    m = Maintenance.create!({comment: "test", start: nil, duration: nil})
    assert m.in
    m.delete
  end

  test "Inside a 90min maintenance" do
    now = Time.now
    m = Maintenance.create!({comment: "test", start: now - (10*3600),
                             duration: 90})
    assert m.in
    m.delete
  end

  test "Outside a 90min maintenance" do
    now = Time.now
    m = Maintenance.create!({comment: "test", start: now - (95*3600),
                             duration: 90})
    assert_not m.in
    m.delete
  end

end
