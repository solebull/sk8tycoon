require 'test_helper'

# The skater views test

class SkaterViewsTest < ActionDispatch::IntegrationTest
  test "should have a Hire me! button if he doesn't belong to a user" do
    skater = Skater.first
    get skater_url(skater)
    
    assert_select "button#validForm", /Hire me now!/
  end

  test "should have a belongs to text if he belongs to a user" do
    skater = Skater.first
    skater.user = User.first
    skater.save!
    get skater_url(skater)

    assert_select ".owner", /belongs to/
  end
end
