require 'test_helper'

# The home page test

class HomeTest < ActionDispatch::IntegrationTest
  test "should contain login form" do
    get root_path
    assert_select 'form#login-form'
    assert_select "a[href=?]", signup_path
  end
end
