require 'test_helper'

class CityControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get cities_url
    assert_response :success
  end

  # testing cities/:id
  test "should get show for a given index" do
    c = City.all.sample
    get cities_url, params: { id: c.id }
    assert_response :success
  end

  test "should get new form" do
    get new_city_url
    assert_response :success
  end
  
end
