require 'test_helper'

class AdminControllerTest < ActionDispatch::IntegrationTest
  test "should get bot" do
    get admin_bot_url
    assert_response :success
  end

  # Just to test the route
  # Was missing in a previous revision
  test "should get start_season" do
    s = Season.first
    get "/admin/start_season/#{s.id}"
    assert_response :redirect # Code 302: Found
  end

  test "should get mailer_test" do
    get admin_mailer_test_url
    assert_response :success
  end

end
