require 'test_helper'

class SkaterControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get skaters_url
    assert_response :success
  end

  # testing skaters/:id
  test "should get show for a given index" do
    s = Skater.all.sample
    get skaters_url, params: { id: s.id }
    assert_response :success
  end

  test "should get new form" do
    get new_skater_url
    assert_response :success
  end

end
