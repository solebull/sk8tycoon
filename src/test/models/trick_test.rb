require 'test_helper'

class TrickTest < ActiveSupport::TestCase
  test "to_a should return an array" do
    #   assert true
    assert_kind_of Array, Trick.to_a
  end

  test "to_a array size must be the same as fixtures number" do
    assert_equal Trick.to_a.length, Trick.all.length
  end

  test "first to_a must be first trick name" do
    assert_equal Trick.to_a[0], Trick.first.name
  end
end
