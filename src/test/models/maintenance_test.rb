require 'test_helper'

class MaintenanceTest < ActiveSupport::TestCase
  test "next should return something" do
    m = Maintenance.new(:start => Time.new + 3.hour)
    m.save
    assert_not_nil Maintenance.next()
  end

  test "next return is sorted" do
    m = Maintenance.new(:start => Time.new + 3.day)
    m.save
    m2 = Maintenance.new(:start => Time.new + 3.hour)
    m2.save
    assert Maintenance.next() == m2
  end

end
