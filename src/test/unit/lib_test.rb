# A file to test add src/lib files

require 'test_helper'

## The class to test src/lib/engine.rb
class LibEngineTest < ActiveSupport::TestCase
  # Just test we can instantiate an engine
  test "can create Engine" do
    e = Engine.new([], 3, 1.1)
    assert_not_nil e
  end

  test "ranking() should have a not nil @players array" do
    e = Engine.new([Skater.random!, Skater.random!], 3, 1.1)
    e2 = Engine.new([], 2, 1.1)
    result = e2.readable(e.to_s)
    result.ranking
  end

  test "Running unknown Engine version should raise an exception" do
    assert_raises(Exception) { Engine.new([], 2, 5.5) }
  end

  test "to_json should return non-nil value" do
    e = Engine.new([Skater.random!, Skater.random!], 3, 1.1)
    e2 = Engine.new([], 2, 1.1)
    result = e2.readable(e.to_s)
    assert_not_nil result.to_json
  end
end
