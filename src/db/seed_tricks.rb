

# For instance, we do not style them (vert street freestyle)
def add_trick(name, diff)
  Trick.create!(name: name, diff: diff,
                vert: 150, street: 150, freestyle: 150)
  
end

def seed_tricks
  puts "Seeding tricks"
  Trick.delete_all
  
  tricks = {"Ollie" => 25,
            "Kickflip" => 50,
            "Heelflip" => 55,
            "Pop Shove It" => 40,
            "FS 180 Ollie" => 75,
            "BS 180 Ollie" => 85,
            "FS Pop Shove It" => 90,
            "No Comply 180" => 75, 
            "Varial Heelflip" => 75, 
            "Nollie Kickflip" => 75, 
            "Nollie Heelflip" => 75, 
            "FS 180 Kickflip" => 75, 
            "BS 180 Kickflip" => 75, 
            "BS 180 Heelflip" => 75, 
            "Switch Heelflip" => 75, 
            "Switch Kickflip" => 75, 
            "360 Pop Shove It" => 75, 
            "BS Bigspin" => 75, 
            "360 Flip" => 75, 
            "Treflip" => 75, 
            "Hardflip" => 80}
  tricks.each do |key, value|
    add_trick(key, value)
  end
end
