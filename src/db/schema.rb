# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20210618172238) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cities", id: :serial, force: :cascade do |t|
    t.string "name"
    t.integer "geoip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "country_id"
  end

  create_table "competition_levels", force: :cascade do |t|
    t.string "short"
    t.string "name"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "competitions", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.integer "start_week"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "countries", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "country_code"
    t.integer "population", default: 0
    t.integer "population_year"
    t.float "hdi"
    t.integer "hdi_year"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "engine_results", force: :cascade do |t|
    t.datetime "started"
    t.datetime "generated"
    t.decimal "engine_version", precision: 5, scale: 2
    t.binary "result_string"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "runs"
  end

  create_table "first_names", force: :cascade do |t|
    t.text "caption"
    t.text "sex"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "country_id"
  end

  create_table "last_names", force: :cascade do |t|
    t.text "caption"
    t.integer "country_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "maintenances", force: :cascade do |t|
    t.string "comment"
    t.integer "start"
    t.integer "duration"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "seasons", force: :cascade do |t|
    t.bigint "start"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "num"
  end

  create_table "skaters", id: :serial, force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.integer "age"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer "freestyle"
    t.integer "street"
    t.integer "vert"
    t.integer "hometown_id"
    t.text "bio_text"
    t.integer "country_id"
    t.text "sex"
    t.integer "user_id"
  end

  create_table "tricks", force: :cascade do |t|
    t.string "name"
    t.integer "diff"
    t.integer "street"
    t.integer "vert"
    t.integer "freestyle"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.decimal "grip", precision: 10, scale: 2
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "remember_digest"
    t.string "activation_digest"
    t.boolean "activated", default: false
    t.datetime "activated_at"
    t.boolean "admin", default: false
    t.index ["email"], name: "index_users_on_email", unique: true
  end

end
