class AddSurfaceStatsToSkater < ActiveRecord::Migration[4.2]
  def up
    add_column :skaters, :freestyle, :integer
    add_column :skaters, :street, :integer
    add_column :skaters, :vert, :integer
  end

  def down
    remove_column :skaters, :freestyle
    remove_column :skaters, :street
    remove_column :skaters, :vert
  end
end
