class AddHometownIdAndProfileText < ActiveRecord::Migration[4.2]
  def up
    add_column :skaters, :hometown_id, :integer
    add_column :skaters, :bio_text, :text
  end

  def down
    remove_column :skaters, :hometown_id
    remove_column :skaters, :bio_text
  end
end
