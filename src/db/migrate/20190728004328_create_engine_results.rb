class CreateEngineResults < ActiveRecord::Migration[5.1]
  def change
    create_table :engine_results do |t|
      t.datetime :started # The start epoch for the competition
      t.datetime :generated
      t.decimal  :engine_version, :precision => 5, :scale => 2
      t.binary   :result_string

      t.timestamps
    end
  end
end
