class CreateMaintenances < ActiveRecord::Migration[5.1]
  def change
    create_table :maintenances do |t|
      t.string :comment
      t.integer :start
      t.integer :duration

      t.timestamps
    end
  end
end
