class CreateCompetitionLevels < ActiveRecord::Migration[5.1]
  def change
    create_table :competition_levels do |t|
      t.string :short
      t.string :name
      t.text :description

      t.timestamps
    end
  end
end
