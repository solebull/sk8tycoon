class CreateCompetitions < ActiveRecord::Migration[5.1]
  def change
    create_table :competitions do |t|
      t.string :name, null: false
      t.text :description
      t.integer :start_week

      t.timestamps
    end
  end
end
