class CreateFirstNames < ActiveRecord::Migration[5.1]
  def change
    create_table :first_names do |t|
      t.text :caption
      t.text :sex

      t.timestamps
    end
  end
end
