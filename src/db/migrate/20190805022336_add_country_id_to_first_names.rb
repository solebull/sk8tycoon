class AddCountryIdToFirstNames < ActiveRecord::Migration[5.1]
  def change
    add_column :first_names, :country_id, :integer
  end
end
