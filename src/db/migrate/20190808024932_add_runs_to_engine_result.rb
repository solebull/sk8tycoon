class AddRunsToEngineResult < ActiveRecord::Migration[5.1]
  def change
    add_column :engine_results, :runs, :integer
  end
end
