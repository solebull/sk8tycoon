class CreateSkaters < ActiveRecord::Migration[4.2]
  def change
    create_table :skaters do |t|
      t.string :first_name
      t.string :last_name
      t.integer :age

      t.timestamps
    end
  end
end
