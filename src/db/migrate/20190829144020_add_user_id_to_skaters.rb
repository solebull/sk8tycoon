class AddUserIdToSkaters < ActiveRecord::Migration[5.1]
  def change
    add_column :skaters, :user_id, :integer, default: nil
  end
end
