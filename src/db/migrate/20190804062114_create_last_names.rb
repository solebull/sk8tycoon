class CreateLastNames < ActiveRecord::Migration[5.1]
  def change
    create_table :last_names do |t|
      t.text :caption
      t.integer :country_id

      t.timestamps
    end
  end
end
