class CreateCountries < ActiveRecord::Migration[4.2]
  def change
    create_table :countries do |t|
      t.string :name
      t.string :country_code
      t.integer :population, :default => 0.0
      t.integer :population_year, :default => nil
      t.float   :hdi, :default => nil # Human Development Index
      t.integer :hdi_year
      t.timestamps
    end
  end
end
