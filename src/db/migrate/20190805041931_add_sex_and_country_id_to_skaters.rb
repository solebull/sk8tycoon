class AddSexAndCountryIdToSkaters < ActiveRecord::Migration[5.1]
  def change
    add_column :skaters, :country_id, :integer
    add_column :skaters, :sex, :text
  end
end
