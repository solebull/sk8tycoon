class CreateTricks < ActiveRecord::Migration[5.1]
  def change
    create_table :tricks do |t|
      t.string :name
      t.integer :diff
      t.integer :street
      t.integer :vert
      t.integer :freestyle

      t.timestamps
    end
  end
end
