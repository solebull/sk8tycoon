
def seed_users
  # Fake accounts
  puts "Creating fake users"
  
  99.times do |n|
    name  = Faker::Name.name
    email = "example-#{n+1}@railstutorial.org"
    password = "password"
    User.create!(name:  name,
                 email: email,
                 grip: 20,
                 password:              password,
                 password_confirmation: password,
                 activated: true,
                 activated_at: Time.zone.now)
  end

  file = File.join(Rails.root, 'db', 'seed_admin.rb')
  require_relative 'seed_admin' if File.exists? file
end
