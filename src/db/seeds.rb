# coding: utf-8
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


require_relative 'seed_countries'
require_relative 'seed_tricks'
require_relative 'seed_users'

module Hdi
  # A typical value (world average) for country we can't the value for
  # Set it if you can't find a country's HDI and set hdi_year to "nil"
  NC = 0.637
end

def feed_firstname(country_code, sex, name_array)
  cid = Country.find_by(country_code: country_code)
  raise "Can't find country with code #{country_code}" if not cid
  name_array.each do |n|
    FirstName.create({caption: n, sex: sex, country_id: cid.id})
  end
end

def feed_lastname(country_code, name_array)
  cid = Country.find_by(country_code: country_code)
  raise "Can't find country with code #{country_code}" if not cid
  name_array.each do |n|
    LastName.create({:caption => n, :country_id => cid.id})
  end
end

# Users
User.delete_all
seed_users

seed_countries

puts "Creating example cities"
City.delete_all
c = City.new(:name => "Plalbio")
c.save!

c2 = City.new(:name => "Albiol")
c2.save!
c3 = City.new(:name => "Dralbuze")
c3.save!



# Initial season
puts "Create initial season (must be started by admin)"
Season.delete_all
Season.create({num: 1, start: 0})


# First Names
puts "Feed First Names"
FirstName.delete_all
LastName.delete_all
fname_female = ["Emma",
                "Lea",
                "Anais",
                "Ambre",
                "Juliette",
                "Eva",
                "Charlotte",
                "Clemence",
                "Maeva",
                "Lena",
                "Pauline",
                "Laura",
                "Jeanne",
                "Julie", 
                "Ines",
                "Louise",
                "Lucie",
                "Manon",
                "Romane",
                "Mathilde",
                "Noemie",
                "Zoe",
                "Lina", "Lisa",
                "Maëlys", "Adele", "Alice", "Anna",
                "Apolline", "Candice",
                "Alicia",
                "Andrea",
                "Audrey",
                "Eloise",
                "Clementine",
                "Elena",
                "Angele","Agathe", "Alexia", "Amandine", "Angelina",
                "Axelle", "Amelie",
                "Capucine", "Constance",
                "Chiara", "Claire", "Coralie", "Elsa",
                "Flora", "Gabrielle", "Flavie"]
                
fname_male =  [ "Enzo",
                "Mathis",
                "Nathan",
         "Axel",
         "Adrien",
         "Baptiste",
         "Arthur",
         "Alexis",
         "Antoine",
         "Evan",
         "Matteo",
         "Nicolas",
         "Gabriel",
         "Julien",
         "Leo",
         "Mael",
         "Valentin",
         "Maxime",
         "Pierre",
         "Quentin",
         "Louis", "Raphael",
         "Romain", "Paul", "Theo", "Tom", "Ethan",
         "Matheo", "Kylian",
         "Alex", "Augustin",  
         "Arnaud", "Aaron", "Alix",
         "Antonin",
         "Diego", "Emilien", "Esteban", 
         "Damien", "Erwan", "Charles",
         "Anthony", "Ayoub", "Bastien", "Alan", 
         "Aymeric", "Bryan", "Alban", "Aurelien",
         "Benjamin", "Gabin",
         "Elias"]

feed_firstname('FRA', 'F', fname_female)
feed_firstname('FRA', 'M', fname_male)

feed_firstname('RUS', 'F', ["Anushka" ,"Anya" ,"Anzhela" ,"Anzhelina",  
                            "Apollinariya" ,"Arina" ,"Arisha" ,"Asya", 
                            "Avdotya" ,"Boleslava" ,"Darya",	"Dunya",  
                            "Dunyasha" ,"Elizaveta" ,"Esfir" ,"Evpraksiya",
                            "Faina" ,"Feodora" ,"Filippa" ,"Galina",
                            "Galya" ,"Gavriila" ,"Gennadiya" ,"Grusha",
                            "Inna" ,"Irinushka" ,"Ivanna" ,"Jekaterina",  
                            "Katenka" ,"Katya" ,"Kira", "Klava",
                            "Klavdiya" ,"Kseniya" ,"Lesya" ,"Lidiya",  
                            "Lidochka" ,"Liliya" ,"Lilya" ,"Liouba",
                            "Lizaveta" ,"Luba" ,"Lyuba" ,"Lyubochka",  
                            "Lyubov" ,"Lyudmila" ,"Manya" ,"Marfa",
                            "Mariya" ,"Marya" ,"Maryana" ,"Masha",
                            "Matrona" ,"Matryona" ,"Motya" ,"Nadejda",
                            "Nadezhda" ,"Nadya" ,"Nastasia", "Nastasya"])

feed_firstname('RUS', 'M', [
                 "Afanasiy", "Afanasy", "Aleksandr", "Aleksei", 
                 "Aleksey", "Alexei", "Alexey", "Alyosha",
                 "Anastas", "Anastasiy", "Anatoli", "Anatoliy", 
                 "Anatoly", "Andrey", "Anisim", "Aristarkh", 
                 "Arkadi", "Arkadiy", "Arkady", "Arkhip", 
                 "Arseni", "Arseniy", "Artyom", "Boleslav",
                 "Borya", "Daniil", "Demyan", "Desya", 
                 "Dimitri", "Dmitri", "Dmitrii", "Dmitriy", 
                 "Dmitry", "Dorofei", "Dorofey", "Ermolai", 
                 "Evgeniy", "Evgeny", "Faddei", "Faddey", 
                 "Fedor", "Fedot", "Fedya", "Feliks", 
                 "Féodor", "Feodosiy", "Feofan", "Feofil",
                 "Feofilakt", "Ferapont", "Filat", "Filipp ",
                 "Fima", "Foka", "Foma", "Fyodor", 
                 "Gavriil", "Gennadi", "Gennadiy", "Gennady", 
                 "Genya", "Georgiy", "Georgy", "Gerasim", 
                 "Germogen", "Gleb", "Grigori", "Grigoriy", 
                 "Grigory", "Grisha", "Ilari", "Ilia", 
                 "Illarion", "Ilya", "Innokenti", "Innokentiy", 
                 "Ioann", "Iosif", "Ipati", "Ippolit", 
                 "Irinei", "Iriney", "Isaak", "Isay", 
                 "Karp", "Kenya", "Khariton", "Kir", 
                 "Kirill", "Kliment", "Kolya", "Kostya ",
                 "Kuzma", "Lavrenti", "Lavrentiy", "Lavrenty", 
                 "Léonid", "Leonti", "Leonty", "Lev", 
                 "Luka", "Lyosha", "Lyov", "Makar", 
                 "Makari", "Makariy", "Maks", "Maksim ",
                 "Maksimilian", "Marlen", "Matfey", "Matvei", 
                 "Matvey", "Maxim", "Mefodiy", "Melor", 
                 "Mikhail", "Miron", "Misha", "Mitrofan", 
                 "Mitya", "Modest", "Modya", "Moisey", 
                 "Naum", "Nazar", "Nazariy", "Nikifor", 
                 "Nikodim", "Nikolai", "Oleg", "Onisim", 
                 "Osip", "Pankrati", "Pasha", "Patya", 
                 "Pavel", "Radimir", "Rodion", "Rodya", 
                 "Rolan", "Roma", "Rostislav", "Rurik"])

feed_firstname('KHM', 'M', [
                 "Akhara", 
                 "Chamroeun", 
                 "Chetha", 
                 "Comsort", 
                 "Dara", 
                 "Hout", 
                 "Kang", 
                 "Kata", 
                 "Katagnou", 
                 "Katha", 
                 "Kavey", 
                 "Keo", 
                 "Keth", 
                 "Khay", 
                 "Kheda", 
                 "Khemarak", 
                 "Khim", 
                 "Kong", 
                 "Kosal", 
                 "Koun", 
                 "Liv", 
                 "Ly", 
                 "Pagna", 
                 "Phoum", 
                 "Rasmey", 
                 "Rotha", 
                 "Samat", 
                 "Samay", 
                 "Sambath", 
                 "Sambo", 
                 "Sambot", 
                 "Samnang", 
                 "Samreth", 
                 "Sarado", 
                 "Seam", 
                 "Sean", 
                 "Seang", 
                 "Seima", 
                 "Serei", 
                 "Sethy", 
                 "Sitthy", 
                 "Socheat", 
                 "Sophal", 
                 "Sophichan", 
                 "Sothea", 
                 "Sothon", 
                 "Sothorm", 
                 "Sothy", 
                 "Sotun", 
                 "Sutho", 
                 "Touch", 
                 "Vanna", 
                 "Vassa", 
                 "Vath", 
                 "Veasna", 
                 "Vichea", 
                 "Vicheat", 
                 "Vicheayin", 
                 "Vichey", 
                 "Virak", 
                 "Vityea", 
                 "Vong", 
                 "Vorak", 
                 "Voromann", 
                 "Yeam", 
                 "Yearn"])       

feed_firstname('KHM', 'F', [
                 "Bopha", 
                 "Chanthou", 
                 "Chenda", 
                 "Gnoap", 
                 "Kaneka", 
                 "Kanitha", 
                 "Kaseng", 
                 "Kateka", 
                 "Katha", 
                 "Keo", 
                 "Keth", 
                 "Kheda", 
                 "Koliane", 
                 "Konthea", 
                 "Kânya", 
                 "Lakhena", 
                 "Mom", 
                 "Neang", 
                 "Phalla", 
                 "Phaneth", 
                 "Pisey", 
                 "Rasmey", 
                 "Ratiana", 
                 "Rotha", 
                 "Seila", 
                 "Seima", 
                 "Seyha", 
                 "Sophal", 
                 "Sophea", 
                 "Sopheap", 
                 "Sophichan", 
                 "Sothy", 
                 "Sotun", 
                 "Sothon", 
                 "Sovandara", 
                 "Sovann", 
                 "Sovannari", 
                 "Sovantevy", 
                 "Srey", 
                 "Surya", 
                 "Sorya", 
                 "Tevy", 
                 "Thevear", 
                 "Thida", 
                 "Vassa", 
                 "Veasna", 
                 "Vichara", 
                 "Virak"])

# from http://www.studentsoftheworld.info/penpals/stats.php3?Pays=ARU
feed_firstname('ABW', 'F', [
                 'camila',
                 'Anne',
                 'Alle',
                 'Munevver Ergun',
                 'Nicki',
                 'Natasha',
                 'Amal',
                 'Manon',
                 'Yanina'])

feed_firstname('ABW', 'M', [
                 'Johm',
                 'Irving',
                 'Biob',
                 'Esmar',
                 'Sean',
                 'George',
                 'Bill',
                 'KR',
                 'Harry',
                 'Estefania',
                 'Fort'])



# LastNames
LastName.delete_all
puts "Feeding LastName table"
feed_lastname("FRA",["Martin",
                     "Bernard",
                     "Thomas",
                     "Petit",
                     "Robert",
                     "Richard",
                     "Durand",
                     "Dubois"])

feed_lastname("RUS", ["Andreï", 
                      "Andropov", 
                      "Antonovitch", 
                      "Armyanski", 
                      "Bobritschew", 
                      "Brobroff", 
                      "Bogdanovitch", 
                      "Bogoliubski", 
                      "Bolchoï", 
                      "Bolgarov", 
                      "Bolgarski", 
                      "Bolotine", 
                      "Korolenko", 
                      "Kosloff", 
                      "Kourine", 
                      "Koutovski", 
                      "Kovalevski", 
                      "Krasine", 
                      "Petroff", 
                      "Petrov", 
                      "Petrovitch", 
                      "Petry", 
                      "Plechkanov", 
                      "Polotski", 
                      "Siline", 
                      "Silitch", 
                      "Simonov", 
                      "Smirnov", 
                      "Smolenski", 
                      "Sokolov", 
                      "Soloviev", 
                      "Tchaïkowski", 
                      "Tchaikovsky", 
                      "Tcherepnine", 
                      "Tcherkasski", 
                      "Tcherkassov", 
                      "Tchernia", 
                      "Tchernine", 
                      "Tiourine", 
                      "Tolstoï", 
                      "Topaloff", 
                      "Verevkine", 
                      "Vesel", 
                      "Vessel", 
                      "Vinogradov", 
                      "Vichniakoff", 
                      "Volk", 
                      "Volonski", 
                      "Zamok", 
                      "Znatchko", 
                      "Zolotarev"])

feed_lastname("KHM",["Kuy", "Kem", "Kep", "Keo", "Khat", "Chhay",
                     "Duong", "Tang", "Phan", "Seoung", "Long"])

feed_lastname('ABW', ['De Jong',
                      'Jansen',
                      'De Vries',
                      'Van de Berg',
                      'Van den Berg',
                      'Van der Berg',
                      'Van Dijk',
                      'Bakker',
                      'Janssen',
                      'Visser',
                      'Smit',
                      'Meijer',
                      'Meyer'])
                      


puts "Populating cities"
City.delete_all
cities_array = {'FRA'=> [
                  'Paris', 
                  'Lyon', 
                  'Marseille', 
                  'Aix-en-Provence', 
                  'Toulouse', 
                  'Bordeaux', 
                  'Lille', 
                  'Nice', 
                  'Nantes', 
                  'Strasbourg', 
                  'Rennes', 
                  'Grenoble', 
                  'Rouen', 
                  'Toulon', 
                  'Montpellier', 
                  'Douai', 
                  'Lens', 
                  'Avignon', 
                  'Saint-Étienne'],
                'ABW' => [
                  'Oranjestad',
                  'Sint Nicolaas',
                  'Santa Cruz',
                  ' Noord'
                ],
                'RUS' => [
                  'Moscow', 
                  'Rostov-na-Donu', 
                  'Novosibirsk', 
                  'Yekaterinburg', 
                  'Saint Petersburg', 
                  'Krasnoyarsk', 
                  'Novosibirsk', 
                  'Perm Krai', 
                  'Yekaterinburg', 
                  'Voronezh', 
                  'Nizhny Novgorod', 
                  'Volgograd', 
                  'Kazan', 
                  'Krasnodar', 
                  'Chelyabinsk', 
                  'Saratov', 
                  'Omsk', 
                  'Tolyatti', 
                  'Samara', 
                  'Izhevsk'
                ],
                'AFG' => [
                  'Kabul', 
                  'Kandahar', 
                  'Herat', 
                  'Mazar-i-Sharif', 
                  'Jalalabad', 
                  'Kunduz', 
                  'Ghazni', 
                  'Lashkargah', 
                  'Taloqan', 
                  'Puli Khumri', 
                  'Khost', 
                  'Charikar', 
                  'Sheberghan', 
                  'Sar-e Pol', 
                  'Maymana', 
                  'Chaghcharan', 
                  'Mihtarlam', 
                  'Farah', 
                  'Puli Alam', 

                ]
               }

cities_array.each do |country_code, cities|
  cid = Country.find_by(country_code: country_code).id
  raise "Can't find country with code #{country_code}" if not cid
  cities.each do |c|
    City.create({name: c, country_id: cid})
  end
end

puts "Creating example skaters"
Skater.delete_all
10.times do
  Skater.random!
end

seed_tricks()

# Competition levels
cl = [
  ['J5', 'Junior 5', '-18 players only. Lowest possible level'],
  ['J4', 'Junior 4', '-18 players only.'],
  ['J3', 'Junior 3', '-18 players only.'],
  ['J2', 'Junior 2', '-18 players only.'],
  ['J1', 'Junior 1', '-18 players only.'],

  ['AMA', 'Amateur', 'Not strong enough to be rabked.'],

  ['FT3', 'Future 3', 'Not yet challengers.'],
  ['FT2', 'Future 2', 'Not yet challengers.'],
  ['FT1', 'Future 1', 'Not yet challengers.'],

  ['CH3', 'Challenger 3', 'More than future. Not yet Master.'],
  ['CH2', 'Challenger 2', 'More than future. Not yet Master.'],
  ['CH1', 'Challenger 1', 'More than future. Not yet Master.'],

  # The amount of points it gives to winner
  ['125', 'Pro 125', 'Very close to master. Already pro.'],
  ['250', 'Pro 250', 'Very close to master. Already pro.'],
  ['500', 'Pro 500', 'Very close to master. Already pro.'],

  ['MST', 'Master 1000', '...'   ],
  ['GS', 'Grand Slam 2500', '...']
]

cl.each do |c|
  CompetitionLevel.create({short: c[0], name: c[1], description: c[2]})
end

# then `rake db:seed`

