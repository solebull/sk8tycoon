# coding: utf-8
# Copyright 2012-2015, 2018-2019 Jérôme Pasquier
#
# This file is part of Sk8tycoon.
# 
# Sk8tycoon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Sk8tycoon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Sk8tycoon.  If not, see <http://www.gnu.org/licenses/>.

# The result of an engine's run
#
# Should be kept at least to read former results


# An olg engine, using array TRICKS
class EngineResult_v1
  TRICKS = ["Ollie", "FS 180 Ollie", "BS 180 Ollie", "Pop Shove It",
          "FS Pop Shove It", "No Comply 180", "Kickflip", "Heelflip",
          "Varial Heelflip", "Nollie Kickflip", "Nollie Heelflip",
          "FS 180 Kickflip", "BS 180 Kickflip", "BS 180 Heelflip",
          "Switch Heelflip", "Switch Kickflip", "360 Pop Shove It",
          "BS Bigspin", "360 Flip", "Treflip", "Hardflip"]

  def initialize()
    @version = 1
    @players = Array.new
    @tricks = Array.new
  end

  def add_player(id)
    @players << id
  end

  def add_trick(id_skater, time, id_trick, luck, quality)
    trick = {:id_skater => id_skater, :time => time, :id_trick =>id_trick,
             :luck =>luck, :quality => quality}
    @tricks << trick
  end
  
  def print
    #puts "VERSION=#{@version}"
    @players.each do |p|
      #puts "  id=#{p}"
    end
  end

  def version
    @version
  end

  def to_json
    # Add name to players
    players = Array.new
    @players.each do |pid|
      phash = Hash.new
      phash[:id] = pid
      p = Skater.find(pid)
      phash[:name] = p.first_name + " " + p.last_name
      players << phash
    end

    # For each trick, add its name
    tr = @tricks
    tr.each do |t|
      t[:trick_name] = TRICKS[t[:id_trick]]
    end
    return {'players' => players, 'tricks' => tr, 'ranking' =>ranking}.to_json
  end
  
  def ranking
    ranks = Hash.new
    @players.each do |p|
      ranks[p] = 0
    end
    
    Skater.find(@players[0]).last_name
    @tricks.each do |t|
      id = t[:id_skater]
      
      score = (100 * t[:luck]/100)
      if t[:quality] == 1 then
        score = score * 1.2
      end
      ranks[id] += score
    end
    sr = ranks.sort_by { |id, score| -score }
    sr
  end
end
