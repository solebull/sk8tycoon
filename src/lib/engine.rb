# coding: utf-8
# Copyright 2012-2015, 2018-2019 Jérôme Pasquier
#
# This file is part of Sk8tycoon.
# 
# Sk8tycoon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Sk8tycoon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Sk8tycoon.  If not, see <http://www.gnu.org/licenses/>.

# The result of an engine's run
#
# Should be kept at least to read former results

include ActionView::Helpers::JavaScriptHelper

require_relative 'old_engines'

class EngineResult_v1_1
  def initialize
    @version = 1.1
    @players = Array.new
    @tricks = Array.new
  end

  def version
    @version
  end
  
  def add_player(id)
    @players << id
  end

  def add_trick(id_skater, time, id_trick, luck, quality)
    trick = {:id_skater => id_skater, :time => time, :id_trick =>id_trick,
             :luck =>luck, :quality => quality}
    @tricks << trick
  end
  
  def to_json
    # Add name to players
    players = Array.new
    @players.each do |pid|
      phash = Hash.new
      phash[:id] = pid
      p = Skater.find(pid)
      phash[:name] = p.first_name + " " + p.last_name
      players << phash
    end

    # For each trick, add its name
    @tricks.each do |t|
      t[:trick_name] = Trick.find(t[:id_trick]).name
    end
    return {'players' => players, 'tricks' => @tricks, 'ranking' =>ranking}.to_json
  end

  def ranking
    raise "players array shoudln't be nil" if @players.nil?
    ranks = Hash.new
    @players.each do |p|
      ranks[p] = 0
    end
    
    Skater.find(@players[0]).last_name
    @tricks.each do |t|
      id = t[:id_skater]
      
      score = (100 * t[:luck]/100)
      if t[:quality] == 1 then
        score = score * 1.2
      end
      ranks[id] += score
    end
    sr = ranks.sort_by { |id, score| -score }
    sr
  end
end

# The Game/Replay engine
#
# Globally, create a EngineResult with the given version and feed it
#
class Engine
#  VERSION = '1' # Actual engine version

  # Engine constructor
  #
  # version should always set to the latest version
  def initialize(skater_array, round, version = 1.1)
    raise "Unknown engine version '#{version}'" if version > 1.1
    
    @version = version
    @nb_runs = round
    @tricks = Trick.to_a
    
    @e = EngineResult_v1_1.new
    
    skater_array.each do |s|
      @e.add_player(s.id)
    end

    # time is in millisecond
    time = 1000
    # Do 5 tricks for each skater
    @nb_runs.times do
      skater_array.each do |d|
        id_trick, luck, qu = do_trick
        #  id, time, trick, result
        @e.add_trick(d.id, time, id_trick, luck, qu)

        time += (rand(1200..3500) / round)

      end
    end

  end

  def do_trick
    id_trick = Trick.all.sample.id
    luck = rand(10000) / 100 # a percentage
    quality=""
    qu = 0
    if luck > 98 then # 98
      quality = "PERFECT"
      qu = 1
    elsif luck < 1.5 then
      quality = "INJURY!"
      qu = -1
      # Must set the player as injuried!
    end
    return id_trick, luck, qu
  end
  
  # To a serializable string
  def print
   puts "RESULT is #{Marshal.dump(@e)}"
  end

  def to_s
    Marshal.dump(@e)
  end
  
  # Save the new result into the database
  def save
    er = EngineResult.create
    er.runs = @nb_runs
    now = Time.now
    er.started = now # Should be the competition start
    er.generated = now 
    er.engine_version = @e.version
    er.result_string = Marshal.dump(@e)
    er.save
  end
  
  # To a human readable text (maybe in JS)
  def readable(str)
    e = nil
    # raise "VERSION >> " + @version.to_s
    if @version == 1
      e = EngineResult_v1
    else
      e = EngineResult_v1_1
    end
    e = Marshal.load(str)
    return e
  end
  
end
