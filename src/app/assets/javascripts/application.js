// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require js.cookie
//= require_tree .

var day_names = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday',
		 'Saturday', 'Sunday'];

/** Pad a number with '0'
  *
  * @param {string}  num The number to be padded
  * @param {integer} size The final string size
  *
  * @return The string paded
  *
  */
function pad(num, size) {
    var s = "000000000" + num;
    return s.substr(s.length-size);
}

/** Update the time every second
  *
  * @param {integer} start_epoch The start epoch of the season.
  *
  */
function update_times(start_epoch) {
    var ts = Math.floor((new Date()).getTime() / 1000);
    var diff = ts - start_epoch;
    var week_duration = 86400;
    var day_duration = week_duration/7;
    var hour_duration = day_duration/24;
    var min_duration = hour_duration/60;
    
    // A week is normally 604800 seconds
    // In the game, a week is a day : 86400 sec.
    var week = Math.floor(diff / week_duration);
    $('#season-week').text(week + 1);

    // Day of the week
    var week_start_in_epoch = start_epoch + (week * week_duration);
    var day = Math.floor((ts - week_start_in_epoch)/day_duration);
    $('#season-day').text(day_names[day]);

    // Hour
    var start_of_day = week_start_in_epoch + (day * day_duration);
    var hour = Math.floor((ts - start_of_day)/hour_duration); // 

    // Minutes
    var start_of_hour = start_of_day + (hour * hour_duration);
    var min = Math.floor((ts-start_of_hour)/min_duration);
    
    $('#season-hour').text(pad(hour, 2) + ":" + pad(min, 2));
    
}

/** Starts to update the seasno widget
  *
  * @param {integer} start_epoch The epoch (from ruby).
  *
  */
function season_widget(start_epoch)
{
    if (start_epoch == 0) {
	$('#season-week').text("Not started");
	$('#season-day').text("Not started");
	$('#season-hour').text("Not started");
    } else {
	update_times(start_epoch)
	setInterval(function(){
	    update_times(start_epoch)
	}, 500);
    }
}
