// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

/** The names used to make the kucky more verbose */
var luck_names = [
    "merveilleux",
    "magnifique",
    "ambitieux",
    "séduisant",
    "élégant",
    "attrayant",
    "intrépide",
    "hésitant",
    "primitif",
    "dangereux",
    "crasseux",
    "hideux"
];

/** Add in real-time a text to the #result element
  *
  * @param text  The text to be added
  * @param close Note : il close is false, you must handle <div> and </div> 
  *
  */
function addToResult(text, close)
{
    var div = document.getElementById('result-text');
    if (close) {
	div.innerHTML += "<div>" + text + "</div>"
    } else {
	div.innerHTML += text;
    }
    
}

/** Add a skater to the #skater-list element
  *
  * @param id   unused ?
  * @param name The name of the skater
  *
  */
function addSkater(id, name)
{
    var div = document.getElementById('skater-list');
    div.innerHTML += '<li>' + name + '</li>';

}

/** Returns the name of a skater from the cometition JSON object
  *
  * @param competJSON The competition JSON object from the server.
  * @param id         Teh skater id.
  *
  * @return The name of the skater or *UNDEFINED NAME* if not find*
  *
  */
function getSkaterName(competJSON, id) {

    var name = "UNDEFINED NAME"
    
    competJSON["players"].forEach(function(p){
	if (p['id'] == id) {
	    name = p['name'];
	}
    });
    return name;
}

/** Retrurns a HTML span with the given text and class
  *
  * bclass is one or more bootstrap class
  *
  * @param bclass The span's class(es).
  * @param text   The span text.
  *
  * @return The complate <span> code.
  *
  */
function span(bclass, text) {
    return "<span class='"+bclass+"'>"+text+"</span>&nbsp;";
}


/** Add a trick to the result div
  *
  * @param compet    The compet JSON object
  * @param skater_id The ID of the skater
  * @param trick     The trick name
  * @param luck      The luck of the trick
  * @param quality   The quality of the trick
  *
  * @return The delay added to full time.
  *
  */
function trick(compet, skater_id, trick, luck, quality) {
    var luck_gap = 100 / luck_names.length;
    var adj_id;

    // If failed, choose a random noun
    if (luck < 10) {
	adj_id = Math.round(Math.random() * 12);
    } else {
	adj_id = Math.round(luck / luck_gap) - 1;
    }

    var t = span("bg-info text-white", getSkaterName(compet, skater_id))
    
	t += "a tenté un " + luck_names[adj_id] + " ";
    t += span("bg-secondary text-white", trick);

    if (luck < 10) {
	addToResult('<div>', false);
	addToResult(t, false);
	
	setTimeout(function(){
	    addToResult(span("bg-secondary text-warning", "CHUTE"), false);
	}, 800);
	if (quality == -1) {
	    setTimeout(function(){
		addToResult(span("", "et se relève"), false);
	    }, 1400);
//	    addToResult('</div>', false);
	    return 1400;
	} else {
	    setTimeout(function(){
		addToResult(span("bg-danger text-white", "et ... se BLESSE!!!"), false);
//		addToResult('</div>', false);
	    }, 2000);
	    return 2000;
	}
	
    }
    else if (quality == 1) {
	console.log("et PERFECT");
	addToResult('<div>', false);
	addToResult(t, false);
	
	setTimeout(function(){
	    addToResult(span("", "et c'est un "), false);
	    addToResult(span("bg-success text-white", "PERFECT!"), false);
//	    addToResult('</div>', false);
	}, 800);

    } else {
	
	addToResult('<div>' + t, false);
    }
    return 0;
}

/** Start the competition
  *
  */
function start() {
    $("#play-button").disabled = true; // Doesn't work!
    document.getElementById("play-button").disabled = true; 

    // init
    addToResult("Competition started");
    //    addSkater();
//    console.log(json_result);
    var compet = JSON.parse(json_result);
    compet["players"].forEach(function(p){
	addSkater(p["id"], getSkaterName(compet, p["id"]));
    });


    var time = 0;
    var delay = 0;
    compet["tricks"].forEach(function(p){
	setTimeout(function(){
	    delay += trick(compet, p['id_skater'], p['trick_name'], p['luck'], p['quality']);
	}, time);

	var score = (100 * p['luck']/100)
	if (p['quality'] == 1 ) {
            score = score * 1.2
	}
	setTimeout(function(){
	    addToResult("Il récolte " + score + " points.</div>", false);
	}, time + delay + 800);
	time = p["time"] + delay + 800;
	

	
    });

    setTimeout(function(){ addToResult("Competition finished", true);}, time);

    // Final result
    setTimeout(function(){
	addToResult("", true);
	addToResult("Resultat final :", true);
	compet["ranking"].forEach(function(r){
	    addToResult("- " + getSkaterName(compet, r[0]) + "&nbsp;:&nbsp;" +
			r[1] + " points", true);
	    
	});

    }, time+1000);
}

/*
$(document).ready(function () {
});
*/
