/** Draws a statistic of a skater with nice looking skateboeards
  * 
  *  @param canvasId The id of the canvas element to draw into
  *  @param value    The value of the stat (0-250)
  *
  */
function draw_stats(canvasId, value)
{
    // Compute width on 123 (image width)
    var w = (value * 123)/250;
    var p= (value * 100)/250;
    var canvas = document.getElementById(canvasId);
    canvas.title=value + "/250 (" + p + "%)";
    var ctx = canvas.getContext('2d');
    if (canvas.getContext) {
	var imgOff = new Image();
	imgOff.onload = function() {
            ctx.drawImage(imgOff, 0, 0);
	};
	imgOff.src = '/assets/stat-off.png';
	
	var imgOn = new Image();
	imgOn.onload = function() {
            ctx.drawImage(imgOn, 0, 0, w, 17, 0, 0, w, 17);
	};
	imgOn.src = '/assets/stat-on.png';
    }
}
