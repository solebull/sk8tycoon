// A $( document ).ready() block.
var skins = ["default", "green"];
var defaultSkin = "green";

/** Remove all body classes then set the selected skin one
 *
 */
function setBodyClass(skinName) {
    var b = $( "body" );
    skins.forEach(function(e){
	b.removeClass(e + '-skin');   
    });
    b.addClass( skinName + '-skin' );
}


$( document ).ready(function() {

    var s= $('#skin');

    // First, feed the skin list
    skins.forEach(function(elem){
	s.append('<option value="' + elem + '">' + elem + '</option>');
    });

    // Then, handle change
    s.change(function () {
	var str = "";
	$( "select option:selected" ).each(function() {
	    str += $( this ).text();
	});

	setBodyClass(str);
	
	// Set cookie
	Cookies.set('skin', str);
    });//.change();

    /* Here we must set the correct option but no need to call
       setBodyclass because it was already set by ruby app helper */
    var skinName = Cookies.get('skin');
    if (skinName === undefined) {
	setBodyClass(defaultSkin);
    } else {
	s.val(skinName);
    }
});
