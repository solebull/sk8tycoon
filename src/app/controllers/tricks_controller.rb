# coding: utf-8
# Copyright 2012-2015, 2018-2019 Jérôme Pasquier
#
# This file is part of Sk8tycoon.
# 
# Sk8tycoon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Sk8tycoon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Sk8tycoon.  If not, see <http://www.gnu.org/licenses/>.

class TricksController < ApplicationController
  before_action :set_trick, only: [:show, :edit, :update, :destroy]

  # GET /tricks
  def index
    @active_menu = "tricks"
    @tricks = Trick.paginate(:page => params[:page], :per_page => 20)
  end

  # GET /tricks/1
  def show
    @active_menu = "tricks"
  end

  # GET /tricks/new
  def new
    @trick = Trick.new
  end

  # GET /tricks/1/edit
  def edit
    @active_menu = "tricks"
  end

  # POST /tricks
  def create
    @trick = Trick.new(trick_params)

    if @trick.save
      redirect_to @trick, notice: 'Trick was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /tricks/1
  def update
    if @trick.update(trick_params)
      redirect_to @trick, notice: 'Trick was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /tricks/1
  def destroy
    @trick.destroy
    redirect_to tricks_url, notice: 'Trick was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_trick
      @trick = Trick.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def trick_params
      params.require(:trick).permit(:name, :diff, :street, :vert, :freestyle)
    end
end
