# coding: utf-8
# Copyright 2012-2015, 2018-2019 Jérôme Pasquier
#
# This file is part of Sk8tycoon.
# 
# Sk8tycoon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Sk8tycoon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Sk8tycoon.  If not, see <http://www.gnu.org/licenses/>.

require "#{Rails.root}/lib/engine.rb"


# The controller for Skater model
class SkatersController < ApplicationController
  # GET /skaters
  # GET /skaters.json
  def index
    @active_menu = "skaters"
    @skaters = Skater.paginate(:page => params[:page], :per_page => 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @skaters }
    end
  end

  # GET /skaters/1
  # GET /skaters/1.json
  def show
    @active_menu = "skaters"
    @skater = Skater.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @skater }
    end
  end

  # GET /skaters/new
  # GET /skaters/new.json
  def new
    @active_menu = "skaters"
    @skater = Skater.new
    @cities = City.all
    
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @skater }
    end
  end

  # GET /skaters/1/edit
  def edit
    @active_menu = "skaters"
    @skater = Skater.find(params[:id])
    @cities = City.all
  end

  # POST /skaters
  # POST /skaters.json
  def create
    @skater = Skater.new(skater_params)

    respond_to do |format|
      if @skater.save
        format.html { redirect_to @skater, notice: 'Skater was successfully created.' }
        format.json { render json: @skater, status: :created, location: @skater }
      else
        format.html { render action: "new" }
        format.json { render json: @skater.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /skaters/1
  # PUT /skaters/1.json
  def update
    @skater = Skater.find(params[:id])

    respond_to do |format|
      if @skater.update_attributes(params[:skater])
        format.html { redirect_to @skater, notice: 'Skater was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @skater.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /skaters/1
  # DELETE /skaters/1.json
  def destroy
    @skater = Skater.find(params[:id])
    @skater.destroy

    respond_to do |format|
      format.html { redirect_to skaters_url }
      format.json { head :no_content }
    end
  end

  # A user hire this skater
  def hire
    @skater = Skater.find(params[:id])
    user = User.find(current_user.id)
    price = params[:price].to_i

    if user.grip < price
      flash[:warning] = "You don't have enough grips to hire this skater.<br>
You only have #{user.grp} grips."
    else
      @skater.user = current_user
      @skater.save!

      user.update_grip! user.grip - price
      
      # Reload current page
      redirect_to skater_path(@skater)
    end
  end

    # You dismiss (stop o controll) this skater
  def dismiss
    @skater = Skater.find(params[:id])
    @skater.user = nil
    @skater.save!
    # Reload current page
    redirect_to skater_path(@skater)
  end

  private

  # A private method to help creating a valid skater once for all
  def skater_params
    params.require(:skater).permit(:first_name, :last_name, :age,
                                   :street, :vert, :freestyle,
                                   :bio_text, :hometown_id)
  end
  
end
