# coding: utf-8
# Copyright 2012-2015, 2018-2019 Jérôme Pasquier
#
# This file is part of Sk8tycoon.
# 
# Sk8tycoon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Sk8tycoon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Sk8tycoon.  If not, see <http://www.gnu.org/licenses/>.

require "#{Rails.root}/lib/engine.rb"

# The home/ controller
class HomeController < ApplicationController

  # Returns the objects used in the index action
  #
  # The actual season, used in _season widet is got directely from Season
  # model (class method).
  #
  def index
    @nb_skaters = Skater.all.size
    @nb_cities = City.all.size
  end

  # A unworking runengine test
  def runengine
    puts "RunEngine is working"
    format.js
  end

  # Return the EngineResult with the given params[:id]
  #
  # We also return the result as JSON for the JS engine.
  def result
    @result = EngineResult.find(params[:id])
    @json_result=@result.to_json;
  end

end
