# coding: utf-8

# coding: utf-8
# Copyright 2012-2015, 2018-2019 Jérôme Pasquier
#
# This file is part of Sk8tycoon.
# 
# Sk8tycoon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Sk8tycoon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Sk8tycoon.  If not, see <http://www.gnu.org/licenses/>.

include MaintenanceHelper # Uses in_maintenance()

# The admin view controller
class AdminController < ApplicationController
  include ApplicationHelper # Uses in_maintenance()
  
  # The index action '/'
  def index
    @active_menu = "admin"
    respond_to do |format|
      format.html # index.html.erb
#      format.json { render json: @countries }
    end
  end

  # Returns Country administration values
  def country
    @active_menu = "admin"
    @country_nb = Country.all.size
    @no_population = Country.all.select{ |c| !c.population_set? }
    @no_cities = Country.all.select{ |c| c.nb_cities == 0 }
    @no_fn = Country.all.select{ |c| c.first_names.count == 0 }
    @no_ln = Country.all.select{ |c| c.last_names.count == 0 }
    
    respond_to do |format|
      format.html # index.html.erb
    end    
  end

  # A view to test the engine
  def engine
    @active_menu = "admin"
    @nb_results = EngineResult.count
    @results = EngineResult.all
    
    respond_to do |format|
      format.html # index.html.erb
      # format.json { render json: @countries }
    end

  end

  # A new Engine result must be created
  def new_engine_result
    nb    = params[:engine][:in].to_i      # Number of skaters
    round = params[:engine][:round].to_i   # Number of round

    if Skater.all.length < nb
      raise "You must have at least #{nb} skaters in DB"
    end

    # Get nb.times  random skater
    skaters = Array.new
    skaters << Skater.find(Skater.pluck(:id).sample)
    
    (nb - 1).times do 
      s = Skater.find(Skater.pluck(:id).sample)
      while skaters.include? s.id do
        puts "!!Skater '#{s.id}' is already in competition (#{skaters.length})"
        s = Skater.find(Skater.pluck(:id).sample)
      end
      skaters << s
    end

    # 2 args, always use latest version
    e = Engine.new(skaters, round)
    e.print
    e.save

    redirect_to action: "engine"
  end

  # Returns the paginated seasons
  def season
    @active_menu = "admin"
    @seasons = Season.paginate(:page => params[:page], :per_page => 20)

  end

  # Start the given season saving the current epoch time
  def start_season
    id = params[:id]
    s = Season.find(id)
    s.start = Time.now.to_i
    s.save!
    redirect_to action: "season"
  end

  # Bot administration values
  def bot
    sk_n = Skater.all.count + 0.0
    bot_n = Skater.where(user: nil).count
    @bot_ratio = (bot_n/sk_n) * 100
    @skaters = Skater.order("created_at DESC").paginate(:page => params[:page], :per_page => 20)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @countries }
    end
    
  end

  # Generate a given number of new Skaters bots
  #
  # Number to be generated is in params[:nb][:in]
  def new_bots
    nb = params[:nb][:in].to_i

    nb.times do
      Skater.random!
    end
    
    redirect_to action: "bot"
  end

  # Create a new non-started season
  def new_season
    last = Season.all.last
    raise "Cannot correctly get last season" if last.nil?

    Season.create!({num: last.num + 1, start: 0})
    redirect_to action: "season"
  end

  def user
    @active_menu = "admin"
    @users = User.paginate(:page => params[:page], :per_page => 20)
    @unactivated_users = User.where({activated_at: nil})
  end

  def mailer_test
  end

  def send_mail_test
    begin
      MailerTestMailer.mailer_test_email(params[:to], params[:msg])
      flash[:success] = "Mail should have been send"

    rescue Net::SMTPAuthenticationError, Net::SMTPServerBusy, Net::SMTPSyntaxError, Net::SMTPFatalError, Net::SMTPUnknownError => e
      flash[:warning] = "Issue sending mail #{e.message}"
    rescue => e
      flash[:error] = "Unknown error #{e.message}"
      

    end
      
    redirect_to action: "mailer_test"
    
  end

  # maintenance admin view
  def maintenance
    @actually = in_maintenance() ? "True" : "False"
    @maintenances = Maintenance.all
    @now = Time.now.to_s
  end

  # Add an instant maintenance : must be manually removed
  def add_instant_maintenance
    Maintenance.create!({comment: params[:Comment], start: nil, duration: nil})
    redirect_to action: "maintenance"
    
  end

  # Add a scheduled maintenance, with start epoch and duration
  def add_scheduled_maintenance
    duration = params[:maintenance]['duration(4i)'].to_i * 3600 +
               params[:maintenance]['duration(5i)'].to_i * 60

    s= params[:maintenance]
    start = Time.new(
      s["start(1i)"],
      s["start(2i)"],
      s["start(3i)"],
      s["start(4i)"],
      s["start(5i)"]
    )
    
    puts "Duration is => #{duration}"
    Maintenance.create!({comment: params[:Comment], start:start,
                         duration: duration})
    redirect_to action: "maintenance"
  end

  def delete_maintenance
    m = Maintenance.find(params[:id])
    m.delete
    redirect_to action: "maintenance"
    
  end
  
end

