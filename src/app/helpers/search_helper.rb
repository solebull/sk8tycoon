# coding: utf-8
# Copyright 2012-2015, 2018-2019 Jérôme Pasquier
#
# This file is part of Sk8tycoon.
# 
# Sk8tycoon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Sk8tycoon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Sk8tycoon.  If not, see <http://www.gnu.org/licenses/>.

module SearchHelper

  # Enable the .each_with_weight sphynx function on a given commection
  #
  # The following quick fix comes from (search for each_with_weight)
  # http://freelancing-gods.com/thinking-sphinx/upgrading.html
  #
  # The each_with_weight function is not anymore by default in 
  # searched objects.
  #
  # see a running version in app/views/search/result.html.erb
  #
  def enable_weight(collection, query)
    res = collection.search(query, :select => '*, weight()')
    res.masks << ThinkingSphinx::Masks::WeightEnumeratorMask
    return res
  end
end
