# The module used as User's helper
module UsersHelper
  # Returns the Gravatar for the given user.
  def gravatar_for(user, options = { size: 80 })
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    size = options[:size]
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar")
  end

  # Has the current user the admin role ?
  #
  # Is not logged in, the user is never admin
  #
  def is_admin?
    if current_user
      current_user.has_role? :admin
    else
      false
    end
  end
end
