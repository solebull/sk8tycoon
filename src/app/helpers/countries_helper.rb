
# The helper of country controller
module CountriesHelper

  # Return the decorated link to this country
  #
  # Country can be a Country object or an id
  def country_link(country)
    return "NIL" if country.nil?

    c = nil
    if country.is_a? Integer then
      c = Country.find(country)
    else
      c = country
    end
    image_tag("#{c.country_code}.png", :alt => c.name) + 
      "&nbsp;".html_safe +  link_to("#{c.name}" , c)
  end
end
