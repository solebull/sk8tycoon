# coding: utf-8
# Copyright 2012-2015, 2018-2019 Jérôme Pasquier
#
# This file is part of Sk8tycoon.
# 
# Sk8tycoon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Sk8tycoon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Sk8tycoon.  If not, see <http://www.gnu.org/licenses/>.

# The City controller helper
module CitiesHelper

  # Return the decorated link to this city
  #
  # Country can be a Country object or an id
  def city_link(city_id)
    return "NIL" if city_id.nil?
    
    c = City.find(city_id)
    cc = Country.find(c.country_id).country_code
    image_tag("#{cc}.png", :alt => c.name) + 
      "&nbsp;".html_safe +  link_to("#{c.name}" , controller: "cities",
                                    action: 'show', id: c.id)
    
  end
end
