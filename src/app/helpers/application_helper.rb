# coding: utf-8
# Copyright 2012-2015, 2018-2019 Jérôme Pasquier
#
# This file is part of Sk8tycoon.
# 
# Sk8tycoon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Sk8tycoon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Sk8tycoon.  If not, see <http://www.gnu.org/licenses/>.

# The Application helper
module ApplicationHelper

  # An helper to print active menu item
  #
  # The item should come from @active_menu, set in controller
  def menu_item(item, text, path)

    active = ""
    if not item.nil? and item.casecmp(text) == 0 then
      active = "active"
    end
    
    link = link_to(text, path)
    "<li class='nav-item #{active}'>#{link}</li>".html_safe
  end

  # A basic submit with a price in grip
  #
  # The price can be found in the params[:price] value from the controller
  # so you dont' have it hardcoded twice
  def grip_button(price, text)
    "<button name='button' type='submit' class='btn btn-primary' id='validForm'>
    #{text} <span class='badge badge-light'>#{price} grips</span>
    </button><input type='hidden' id='price' name='price' 
    value='#{price}'>".html_safe
  end

  def skin_class()
    s = cookies[:skin]

    if s then
      s + "-skin"
    else
      "default-skin"
    end
  end
end
