# New season is created with NULL start epoch
# admin must manually start it in admin pages
class Season < ApplicationRecord
  # start : the POSIX epoch start time 

  # Returns the 'start' epoch timestamp as real date
  def start_date
    return 0 if start.nil?
      
    DateTime.strptime(start.to_s, '%s')
  end

  # Return true if this season is the actual one
  #
  # @return true if this season is the actual one
  def current?
    ac = Season.actual
    return ac.id == self.id
  end


  #  ================ HERE START THE CLASS METHODS =========
  
  # Get the latest created season
  def self.actual
    ret =  Season.where("start != 0").order("created_at").last
    if ret.nil?
      return Season.order("created_at").last
    else
      return ret
    end
  end

  # Returns a HTML error message if the actual season can't be retrieved
  def self.seed_error
    "We can't find <tt>Season.actual</tt>. Maybe you forgot to call 
    <tt>bin/rails db:seed</tt> ?".html_safe
  end
end
