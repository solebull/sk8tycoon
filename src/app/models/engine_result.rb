require "#{Rails.root}/lib/engine.rb"

# The EngineResult model
class EngineResult < ApplicationRecord
  # Return the result ranking ad a HTML safe string
  def ranking
    e = Engine.new([], 5, self.engine_version)
    result = e.readable(self.result_string)

    sr = result.ranking
    ret = ""
    first = true
    sr.each do |p|
      sk = Skater.find(p[0])
      ret2 = "#{sk.first_name} #{sk.last_name}: #{p[1]} points<br>"
      if first then
        ret += "<b>#{ret2}</b>"
        first = false
      else
        ret += "#{ret2}"
      end

    end
    ret.html_safe
  end

  # Return the Engine Result as JSON
  #
  # Override to_json function
  def to_json
    e = Engine.new([], 0)
    result = e.readable(self.result_string)

    return result.to_json
  end
end
