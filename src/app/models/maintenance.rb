class Maintenance < ApplicationRecord

  # Duration is stored in minutes

  # Return true if this maintenance is active
  def in
    # Instant maintenance, always on
    return true if self.start.nil? and self.duration.nil?

    if not self.start.nil?
      to = self.start + (self.duration * 1000)
      now = Time.now.to_i
      #puts start, to, now
      return true if now > self.start and now < to
    end

    return false
  end

  # Return the next maintenance window, sorted by time
  def self.next
    return Maintenance.order(:start).first
  end
  
end
