# coding: utf-8
# Copyright 2012-2015, 2018-2019 Jérôme Pasquier
#
# This file is part of Sk8tycoon.
# 
# Sk8tycoon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Sk8tycoon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Sk8tycoon.  If not, see <http://www.gnu.org/licenses/>.

# The Trick model
#
# == Database values
#
#      string :name
#      integer :diff : /255 the overall difficulty for this trick
#      integer :street  /255 Is this a street 
#      integer :vert
#      integer :freestyle
class Trick < ApplicationRecord

  # Returns an array of tricks name
  def self.to_a
    a = Array.new
    Trick.all.each do |t|
      a << t.name
    end
    a
  end
end
