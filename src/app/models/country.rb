# coding: utf-8
# Copyright 2012-2015, 2018-2019 Jérôme Pasquier
#
# This file is part of Sk8tycoon.
# 
# Sk8tycoon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Sk8tycoon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Sk8tycoon.  If not, see <http://www.gnu.org/licenses/>.


# The Country model
class Country < ActiveRecord::Base
#  attr_accessible :country_code, :name, :population, :population_year 
#  attr_accessible :hdi, :hdi_year
  has_many :cities
  has_many :first_names
  has_many :last_names
  has_many :skaters

  # Is the population corretly set (is not nil?)
  def population_set?
    !self.population_year.nil?
  end

  # Is the HDI (Human Development Index) corretly set (is not nil?)
  def hdi_set?
    !self.hdi_year.nil?
  end

  # Returns the population of the country as a string
  def population_str
    ActionController::Base.helpers.number_with_delimiter(self.population, :delimiter => ' ')
  end

  # Returns the number of cities in this Country
  def nb_cities
    cities.count
  end
end
