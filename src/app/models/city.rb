# coding: utf-8
# Copyright 2012-2015, 2018-2019 Jérôme Pasquier
#
# This file is part of Sk8tycoon.
# 
# Sk8tycoon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Sk8tycoon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Sk8tycoon.  If not, see <http://www.gnu.org/licenses/>.

# The City model
#
# Mainly used as hometown for skaters. A City belongs to only one country.
class City < ActiveRecord::Base
  #  attr_accessible :geoip, :name
  has_many :skaters, :foreign_key => 'hometown_id'
  belongs_to :country

  # Returns the number of skaters which have this city as hometown
  def nb_skaters
    Skater.where(hometown_id: self.id).size
  end

  # Give a random city from the given country (or not)
  #
  # There is a very little chance (2%) to get a random city from a different
  # country.
  def self.from_country(country_id)
    r = rand * 99
    if r < 2
      return City.all.sample
    end

    c = City.where({country_id: country_id}).sample
    if c.nil? then
      c = City.all.sample
    end
    return c
  end
end
