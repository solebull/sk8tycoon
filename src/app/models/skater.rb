# coding: utf-8
# Copyright 2012-2015, 2018-2019 Jérôme Pasquier
#
# This file is part of Sk8tycoon.
# 
# Sk8tycoon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Sk8tycoon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Sk8tycoon.  If not, see <http://www.gnu.org/licenses/>.


# The Skater model
class Skater < ActiveRecord::Base
  validates :first_name,  :presence => true
  validates :last_name,  :presence => true
  validates :street, :numericality => { :less_than_or_equal_to => 250 }
  validates :vert, :numericality => { :less_than_or_equal_to => 250 }
  validates :freestyle, :numericality => { :less_than_or_equal_to => 250 }

  belongs_to :city, foreign_key: 'hometown_id'
  belongs_to :country
  belongs_to :user
  
  # Returns this skater's hometown name
  def hometown
    begin
      City.find(self.hometown_id).name
    rescue ActiveRecord::RecordNotFound
      "No hometown found (id = #{self.hometown_id})"
    end
  end

  # Get a random newly-created Skater object
  def self.random!
    l = LastName.all.sample
    f = FirstName.where({country_id: l.country_id}).sample
    raise "Can't find FirstName for country.id #{l.country_id}" if f.nil?
    
    a = (14..35).to_a.sample

    fr = (20..250).to_a.sample
    s = (20..250).to_a.sample
    v = (20..250).to_a.sample

    # 1% of all skaters are 'X'
    se_r = rand * 99
    if se_r < 1
      se = 'X'
    else
      se = f.sex
    end

    # 5% have a random nationality
    nat = nil
    nat_r = rand * 99
    if nat_r < 5
      nat = Country.all.sample.id
    else
      nat = l.country_id
    end

    # Hometown
    #  h = City.all.sample
    h = City.from_country(nat)
    
    Skater.create({first_name: f.caption, last_name: l.caption,
                   age: a, freestyle: fr, street: s, vert: v,
                   hometown_id: h.id, country_id: nat,
                   sex: se})
  end
  
end
