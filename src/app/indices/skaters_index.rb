ThinkingSphinx::Index.define :skater, :with => :active_record do
  indexes [first_name, last_name], :as => :name, :sortable => true
  set_property :field_weights => { :name => 10 }
end
