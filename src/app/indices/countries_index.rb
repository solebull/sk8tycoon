ThinkingSphinx::Index.define :country, :with => :active_record do
  indexes country_code, :sortable => true
  indexes name, :sortable => true
end
