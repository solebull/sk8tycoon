class MailerTestMailer < ApplicationMailer
  def mailer_test_email(to, msg)
    mail(to: to, subject: 'Sk8tycoon mailer test')
  end
end
