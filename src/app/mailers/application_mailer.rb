class ApplicationMailer < ActionMailer::Base
  ADDR = 'noreply@sk8tycoon.org'
  default from: ADDR, reply_to: ADDR
  layout 'mailer'
end

