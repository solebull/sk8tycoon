Rails.application.routes.draw do
  get 'static/help'

  get 'admin/mailer_test'

  resources :tricks
  get 'admin/bot'

  get 'admin/season'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  get 'admin/engine'

  resources :countries
  resources :cities
  resources :skaters
  resources :users

  resources :account_activations, only: [:edit]
  
  get "home/index"
  #  get ':controller(/:action(/:id))(.:format)'
  root :to => "home#index"

  namespace :cities do
    %w(index show).each do |action|
      get action, action: action
    end
  end

  namespace :admin do
    %w(index country user).each do |action|
      get action, action: action
    end
  end

  #namespace :home do
  #  %w(result).each do |action|
  #    get action, action: action
  #  end
  #end

  get '/home/result/:id', to: 'home#result'
  
  match '/search/result', to: 'search#result', via: 'get'
  
  # Handle the new_engine_result action
  match '/admin/new_engine_result', to: 'admin#new_engine_result', via: 'get'
  match '/admin/new_engine_result', to: 'admin#new_engine_result', via: 'post'

  match '/admin/new_bots', to: 'admin#new_bots', via: 'get'
  match '/admin/new_bots', to: 'admin#new_bots', via: 'post'

  match '/admin/new_season', to: 'admin#new_season', via: 'post'
  match '/admin/start_season/:id', to: 'admin#start_season', via: 'get'
  match '/admin/start_season/:id', to: 'admin#start_season', via: 'post'
  match '/admin/send_mail_test', to: 'admin#send_mail_test', via: 'post'

  get    '/admin',                          to: 'admin#index'
  get    '/admin/maintenance',              to: 'admin#maintenance'
  post   '/admin/add_instant_maintenance',  to: 'admin#add_instant_maintenance'
  post   '/admin/add_scheduled_maintenance',to:'admin#add_scheduled_maintenance'
  post  '/admin/delete_maintenance/:id',    to: 'admin#delete_maintenance'

  post '/skater/hire/:id', to: 'skaters#hire'
  post '/skater/dismiss/:id', to: 'skaters#dismiss'

 
  get  '/signup',  to: 'users#new'
  post '/signup',  to: 'users#create'

  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
end
