.PHONY: doc

check:
	cd src/ && make check

doc:
	./rdoc
