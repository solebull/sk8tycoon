/* Starting a simple menu in OO style */
function Menu(x, y)
{
    this.x = x;
    this.y = y;

    var this.childs = new Array();
}

Menu.prototype.addItem = function(text)
{
    this.childs.push("text");
}

Menu.prototype.draw = function(context)
{
    // Must draw menu items
    context.fillStyle = "blue";
    context.font = "bold 16px Arial";
    context.fillText("Menu", self.x, self.y);
}

/* canvasId:: The id of the canvas element to draw into
 * value:: The value of the stat (0-250)
 * 
 * 
 */
function draw_stats(canvasId, playerName, value)
{
    // Compute width on 123 (image width)
    var w = (value * 123)/250;
    var canvas = document.getElementById(canvasId);
    var ctx = canvas.getContext('2d');
    if (canvas.getContext) {
	var imgOff = new Image();
	imgOff.onload = function() {
            ctx.drawImage(imgOff, 0, 0);
	};
	imgOff.src = '../src/app/assets/images/stat-off.png';
	
	var imgOn = new Image();
	imgOn.onload = function() {
            ctx.drawImage(imgOn, 0, 0, w, 17, 0, 0, w, 17);
	};
	imgOn.src = '../src/app/assets/images/stat-on.png';
    }
    canvas.title = playerName + ' (' + value + '/250)'
}

/* canvasId:: The id of the canvas element to draw into
 */
function start_runengine(canvasId)
{
    var canvas = document.getElementById(canvasId);
    var ctx = canvas.getContext('2d');

    ctx.fillStyle = "blue";
    ctx.font = "bold 16px Arial";
    ctx.fillText("Zibri", 100, 100);

    var m = new Menu(10,10);
    m.draw(ctx);
}