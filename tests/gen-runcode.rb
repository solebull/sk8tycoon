#!/usr/bin/ruby

require 'stringio'

# Test the endianness
ENDIAN = ("Ruby".unpack("i")[0] == 2036495698)? "Little": "Big" 
p "#{ENDIAN} endian"

module Action
  WAITMS          = 0 # Wait n[3] milliseconds
  PLAYER          = 1 # Change to n[2] player
  TRICKS          = 2 # Do the trick # n[4], making n[4] points
end

def pack(n, len=1)
  return n.to_s(16).rjust(len, '0')
end


def gencode
  s = ""
  s << pack(Action::WAITMS)
  s << pack(800, 3)
  s << pack(Action::PLAYER)
  s << pack(18, 2)
  s << pack(Action::WAITMS)
  s << pack(200, 3)
  s << pack(Action::TRICKS)
  s << pack(210, 3)

  return s
end

def readcode(c)
  io = StringIO.new(c)
  until (io.eof)
    act = io.read(1).to_i
    case
    when act == Action::WAITMS
      ms = io.read(3).hex
      puts "Waiting #{ms}ms"
    when act == Action::PLAYER
      pl = io.read(2).hex
      puts "Changing to player ##{pl}"
    when act == Action::TRICKS
      tr = io.read(3).hex
      puts "Trick #{tr}"
    end
  end
end

# milliseconds to wait to hex packed in 3 letters hex. 
# MAX is 4095
# p "MAX : ", 'f'.hex, 'ff'.hex, 'fff'.hex, 'ffff'.hex

gc = gencode
p gc

readcode(gc)


