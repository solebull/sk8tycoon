# Sk8tycoon

A skateboard management game playable through a browser. The goal of the game
is to provide a way to manage one or many skateboarders, in competition
and training.

[[_TOC_]]

# Installation

## Installing dependencies

You may need to install some dependencies before running app (as root or
with your account passphrase if sudo is installed) :

	sudo apt install ruby-rails ruby-dev default-libmysqlclient-dev  \
	  postgresql-server-dev-all sphinxsearch libc6-dev postgresql-13 \
	  net-tools

Distribution-agnostic steps :

	cd src/
	bundle config set --local path 'vendor/bundle'
	bundle install

If you're upgrading an existing installation :

	gem pristine --all

may help. It restores the installed gems in the bundle to their pristine 
condition using the local gem cache from RubyGems.

If after the upgrade, it still fails :

	cd src/
	rm -fr  vendor/bundle/
	bundle config set --local path 'vendor/bundle'
	bundle install

should reinstall all dependencies.

## Installing Gemfile's ruby version

	curl -sSL https://get.rvm.io | bash -s stable --ruby

If needed, install the GPG key using the provided command. Something like

	gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys <long id>

Then, install the needed ruby version using `rvm`.

	source /home/rainbru/.rvm/scripts/rvm
	rvm install "ruby-2.7.4"

## Database configuration

Copy the *src/config/database.yml.example* to *src/config/database.yml*.
Edit the *src/config/database.yml* file with postgresql credentials.

Then create postresql user and databse (on Debian GNU/Linux):

	su                   # Enter your admin password
	su - postgres
	psql
	CREATE USER <username> WITH PASSWORD '<password>';
	ALTER USER <username> CREATEDB;
	CREATE DATABASE <dbname> OWNER <username>;
	CREATE DATABASE <testdb_name> OWNER <username>;

If `psql` command can't connect to server, you may use the -p option :
`psql -p5433`.

Ater user is used when resetting database.

Then, go back to a terinal using `\q` command , and from the root 
directory type these commands :

	cd src/
	bin/rake db:migrate
	bin/rake db:seed         # To load initial values (countries etc...)

## Production configuration

Rename src/config/environments/production.rb.example to
src/config/environments/production.rb at least for the SMTP settings.

## Sphynx configuration

In debian, shpinxsearch can be found in jessie-backports.

From the *src* directory, :

	bin/rake ts:index
	bin/rake ts:start

### therubyracer error

Try manually to run `sudo gem install therubyracer -v '0.9.10'`
Trying to install latest version 0.12.3

## Running application locally

You must run this command

	cd src/
	./bin/rails server

Then you can point your prefered browser to URL http://localhost:3000/.

## App documentation

To generate application documentation :

	cd src/
	./rdoc

Then point your favorite browser to `src/doc/` for the ruby documentation.
The JS documentation is in the `src/doc-js/` directory.

## Unit tests

To run unit tests (among others) :

	cd src
	bin/rails test

## Admin user seed

The administration user creation can be seed in a `src/db/seed_admin.rb`.

## Troubleshooting


### Error connecting to Sphinx

If you got the *Error connecting to Sphinx via the MySQL protocol.
Error connecting to Sphinx via the MySQL protocol.* when trying to search
a string, please be sure you run `rake ts:index` then `rake ts:start`.


### PG::ConnectionBad: could not connect to server: Connection refused

When running migrations, if you have a similar error (from 
https://askubuntu.com/q/50621 ):

```
PG::ConnectionBad: could not connect to server: Connection refused
	Is the server running on host "localhost" (::1) and accepting
	TCP/IP connections on port 5433?
could not connect to server: Connection refused
	Is the server running on host "localhost" (127.0.0.1) and accepting
	TCP/IP connections on port 5433?
```

#### Listening port/user error

If you're *postgresql* server is using/listening another TCP port. You can
change it in `src/config/database.yml`. You may also want to verify the listed
user exist and can manually connect to postgres server.


You can make sure you postgres cluster is up and running :

	ps axf | grep postgres
	
if it is, make sure it listens to the 5433 TCP port :

	#netstat -nltp | grep 5433

#### Bad trust method

If not, you may want to search and modify the `pg_hba.conf` file and
change the given method for IPv4 connections from *peer* to **trust** :

	host    all             all             127.0.0.1/32            peer

