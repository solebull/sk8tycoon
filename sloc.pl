#!/usr/bin/perl

use strict;
use warnings 'all';


sub format_int
# returns number with commas.....
# so format_int(12345.667) returns "12,345"
{
    my $num = shift;
    return reverse(join(",",unpack("(A3)*", reverse int($num))));
}

# count the SLOC in the given directory
sub count
{
    my $dir = $_[0];
    print "Computing SLOC for '$dir'\n";
    my $sl=`sloccount $dir 2>/dev/null`;
    $sl =~ /Total Physical Source Lines of Code \(SLOC\) * = (.*)/;
    my $line = $1;
    $line =~ s/,//g;
    return $line;
}

my $src =  count("src/");
my $ven =  count("src/vendor/");


my $diff = format_int($src- $ven + 21); # 21 is myself
print "SLOC are '$diff'\n";
